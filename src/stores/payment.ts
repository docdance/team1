import { defineStore } from 'pinia'
import { ref } from 'vue'

export const usePaymentStore = defineStore('payment', () => {
  const cash = ref(0)
  const pay = ''
  function clear() {
    cash.value = 0
  }
  function qrcode() {
    cash.value = 0
  }
  return { cash, clear, pay }
})
