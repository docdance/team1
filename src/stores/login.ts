import type { User } from '@/types/User'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import { useAuthStore } from './auth'

export const useLoginStore = defineStore('user', () => {
  const users = ref<User[]>([
    {
      id: 1,
      email: 'Pattaradanai@gmail.com',
      password: 'nai1234',
      fullName: 'Pattaradanai Srisarakorn',
      gender: 'female',
      roles: ['manager']
    },
    {
      id: 2,
      email: 'Chanakan@gmail.com',
      password: 'fern1234',
      fullName: 'Chanakan Phanphin',
      gender: 'female',
      roles: ['staff']
    },
    {
      id: 3,
      email: 'Warunthorn@gmail.com',
      password: 'dear1234',
      fullName: 'Warunthorn Suksomloek',
      gender: 'female',
      roles: ['staff']
    },
    {
      id: 4,
      email: 'Phuttiphong@gmail.com',
      password: 'arm1234',
      fullName: 'Phuttiphong Khantasen',
      gender: 'male',
      roles: ['staff']
    },
    {
      id: 5,
      email: 'Kittikorn@gmail.com',
      password: 'Leo1234',
      fullName: 'Kittikorn Foythaworn',
      gender: 'male',
      roles: ['staff']
    }

  ])

  const username = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  const isVisible = ref(false)

  function toggleComponent() {
    isVisible.value = !isVisible.value
  }
  function login() {
    const foundUser = users.value.find(
    (item) => item.email === username.value && item.password === password.value
    )
    if (foundUser) {
      router.push('/pos-view')
      useLoginStore().setLoggedInUser(foundUser.fullName, foundUser.email)
    } else {
      dialogFailed.value = true
      return
    }
    isVisible.value = true
  }


  const loggedInUser = ref<{ name: string; email: string } | null>(null)
  function setLoggedInUser(name: string, email: string) {
    loggedInUser.value = { name, email }
  }

  function closeDialog() {
    dialogFailed.value = false
    clear()
  }
  function clear() {
    username.value = ''
    password.value = ''
    setLoggedInUser('', '')
  }
  function logout() {
    clear()
    router.push('/')
  }

  return {
    users,
    username,
    password,
    loggedInUser,
    dialogFailed,
    login,
    setLoggedInUser,
    closeDialog,
    logout,
    toggleComponent

  }

})