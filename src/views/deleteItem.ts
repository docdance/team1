import type { Salary } from '@/types/Salary';
import { editedIndex, salary, editedSalary, dialogDelete } from './SalaryView.vue';

function deleteItem(item: Salary) {
editedIndex = salary.value.indexOf(item);
editedSalary.value = Object.assign({}, item);
dialogDelete.value = true;
editedIndex = -1;
}
