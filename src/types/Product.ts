type Product = {
  id: number
  name: string
  price: number
  type: number
  sweet: string
}

export { type Product }
