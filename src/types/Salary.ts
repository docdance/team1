type Gender = 'male' | 'female' | 'others'
type Role = 'Manager' | 'Staff'
type Status = 'PartTime' | 'FullTime'
type PaymentStatus = 'Paid' | 'Waiting for Payment'
type Salary = {
  id: number
  name: string
  tel: string
  roles: Role[]  // Manager, Staff
  status: Status[]
  salary: number
  paymentStatus: PaymentStatus[]
}

export type { Gender, Role, Salary , PaymentStatus }