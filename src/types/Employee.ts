type Gender = 'male' | 'female' | 'others'
type Role = 'Manager' | 'Staff'
type Status = 'PartTime' | 'FullTime'
type Employee = {
  id: number,
  name: string,
  tel: string,
  roles: Role[]  // Manager, Staff
  status: Status[],
  checkIn: string,
  checkOut: string,
  salary: number
  date: string
}

export type { Gender, Role, Employee }