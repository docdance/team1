import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import App from '../App.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/login/LoginView.vue')
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('../views/UsersView.vue')
    },
    {
      path: '/pos-view',
      name: 'pos',
      component: () => import('../views/pos/POSView.vue')
    },
    {
      path: '/employee-management',
      name: 'employee',
      component: () => import('../views/EmployeeManagementView.vue')
    },
    {
      path: '/stock-management',
      name: 'stock',
      component: () => import('../views/stock/StockView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/login/LoginView.vue')
    },
    {
      path: '/App',
      name: 'App',
      component: () => import('../App.vue')
    },
    {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/SalaryView.vue')
    },
    {
      path: '/checkinout',
      name: 'checkinout',
      component: () => import('../views/CheckInOutView.vue')
    }
    

  ]
})

export default router
